#pragma once
#include <iostream>
#include <sstream>

class Logger
{
 public:
  enum LogLevel { DEBUG, INFO, WARNING, ERROR };
  std::ostringstream& Get(LogLevel level);
  Logger();
  ~Logger();
 private:
   std::ostringstream os;
   const char* logLevelToString(LogLevel level);
};

#define LOG(level) \
Logger().Get(level)
