#include "Ball.h"
#include "Logger.h"
#include "Engine.h"
#include "VectorMath.h"
#include "math.h"

Ball::Ball(sf::Vector2f spawnPoint) : spawnPoint(spawnPoint)
{
  setVelocityDeceleration(0.0f);
  initialVelocity = sf::Vector2f(-3.5, 5.5);

  if (!texture.loadFromFile("res/ball.png")) LOG(Logger::ERROR) << "Unable to load the ball texture";
  loadSpriteFromTexture();
  setHitbox((sf::Vector2f) texture.getSize());

  respawn();
}

Ball::~Ball()
{
}

void Ball::tickEntity()
{
  wallBounce();
  score();
  paddleCollision();
}

void Ball::render(sf::RenderWindow &rw, float partialTicks)
{
  sprite.setPosition(*getPreviousPosition() + (position - *getPreviousPosition()) * partialTicks);
  rw.draw(sprite);
}

void Ball::wallBounce()
{
  sf::FloatRect hitbox = getHitbox();

  if(hitbox.top < 0) velocity.y = -velocity.y;
  if(hitbox.top + hitbox.height > Engine::getLevel()->getSize().y) velocity.y = -velocity.y;
}

void Ball::score()
{
  if(position.x <= 0 || position.x + getHitbox().width / 2 >= Engine::getLevel()->getSize().x)
  {
    respawn();
  }
}

void Ball::respawn()
{
  initPosition(spawnPoint);
  velocity = initialVelocity;
}

void Ball::paddleCollision()
{
  Paddle* collidedPaddle = nullptr;

  for(Paddle* paddle : Engine::getLevel()->getEntities<Paddle>())
  {
    if(this->isCollidedWith(paddle)) collidedPaddle = paddle;
  }

  if(collidedPaddle && !previouslyCollided)
  {
    if(isCollidedSidewaysWith(collidedPaddle)) sideBounce(collidedPaddle);
    else normalPaddleBounce(collidedPaddle);

    previouslyCollided = true;
  }
  else if(!collidedPaddle && previouslyCollided) previouslyCollided = false;
}

void Ball::sideBounce(Paddle* paddle)
{
  //Top side
  if(position.y < paddle->getPosition()->y)
  {
    if(velocity.y < 0) velocity.y = paddle->getVelocity()->y;
    else velocity.y = paddle->getVelocity()->y - velocity.y;
  }
  //Bottom side
  else
  {
    if(velocity.y > 0) velocity.y = paddle->getVelocity()->y;
    else velocity.y = paddle->getVelocity()->y - velocity.y;
  }
}

void Ball::normalPaddleBounce(Paddle* paddle)
{
  velocity.x = -velocity.x;
  velocity.x *= random.getFloat(1.0f, 1.1f);

  const float factor = random.getFloat(1.1f, 1.2f);

  if(paddle->getVelocity()->y < 0)
  {
    if(velocity.y < 0) velocity *= factor;
    else if(velocity.y > 0) velocity /= factor;
  }
  else if(paddle->getVelocity()->y > 0)
  {
    if(velocity.y > 0) velocity *= factor;
    else if(velocity.y < 0) velocity /= factor;
  }
}

bool Ball::isCollidedSidewaysWith(Paddle* paddle)
{
  if(paddle->isPlayer())
  {
    return getPreviousPosition()->x - getHitbox().width / 2 < paddle->getPosition()->x + paddle->getHitbox().width / 2;
  }
  else
  {
    return getPreviousPosition()->x + getHitbox().width / 2 > paddle->getPosition()->x - paddle->getHitbox().width / 2;
  }

  return false;
}

