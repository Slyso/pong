#include "Random.h"

#include <time.h>
#include <stdlib.h>
#include "Logger.h"

Random::Random()
{
  srand(time(0));
}

Random::~Random()
{
}

bool Random::getBool()
{
  return rand() % 2;
}

int Random::getInt(int max, int min)
{
  return min + rand() % (max - min + 1);
}

float Random::getFloat(float max, float min)
{
  return min + static_cast<float>(rand()) / (static_cast<float>(RAND_MAX / (max - min)));
}
