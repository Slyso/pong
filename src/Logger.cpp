#include "Logger.h"
#include <chrono>
#include <time.h>

Logger::Logger()
{
}

Logger::~Logger()
{
  os << std::endl;
  fprintf(stdout, "%s", os.str().c_str());
}

std::ostringstream& Logger::Get(LogLevel level)
{
  std::chrono::system_clock::time_point now = std::chrono::system_clock::now();
  time_t tt = std::chrono::system_clock::to_time_t(now);
  tm localTm = *localtime(&tt);

  os << localTm.tm_hour << ":" << localTm.tm_min << ":" << localTm.tm_sec << " - [" << logLevelToString(level) << "]\t";
  return os;
}

const char* Logger::logLevelToString(LogLevel level) {
  switch (level) {
    case DEBUG: return "DEBUG";
    case INFO: return "INFO";
    case WARNING: return "WARNING";
    case ERROR: return "ERROR";
    default: return "";
  }
}
